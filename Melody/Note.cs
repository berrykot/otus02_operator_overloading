﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Melody
{
	/// <summary>
	/// Нота/Note
	/// </summary>
	public class Note : ICloneable
	{
		/// <summary>
		/// Частота звука ноты/Freequncy
		/// </summary>
		protected int Frequency { get; set; }

		/// <summary>
		/// Длительность ноты/Duration
		/// </summary>
		protected int Duration { get; set; }

		/// <summary>
		/// Без звука/Mute
		/// </summary>
		public static readonly Note Mute = new Note(0);
		/// <summary>
		/// До/Do
		/// </summary>
		public static readonly Note Do = new Note(261);
		/// <summary>
		/// Рэ/Re
		/// </summary>
		public static readonly Note Re = new Note(293);
		/// <summary>
		/// Ми/Mi
		/// </summary>
		public static readonly Note Mi = new Note(329);
		/// <summary>
		/// Фа/Fa
		/// </summary>
		public static readonly Note Fa = new Note(349);
		/// <summary>
		/// Соль/Sol
		/// </summary>
		public static readonly Note Sol = new Note(392);
		/// <summary>
		/// Ля/La
		/// </summary>
		public static readonly Note La = new Note(440);
		/// <summary>
		/// Си/Si
		/// </summary>
		public static readonly Note Si = new Note(493);

		public Note(int frequency, int duration = 300)
		{
			Frequency = frequency;
			Duration = duration;
		}

		public Note(Note note): this(note?.Frequency ?? 0, note?.Duration ?? 0)
		{
			if (note == null) throw new ArgumentNullException("note");
		}

		/// <summary>
		/// Воспроизвести/Play
		/// </summary>
		public void Play()
		{
			if(!this) Thread.Sleep(Duration);
			else Console.Beep(Frequency, Duration);
		}

		/// <summary>
		/// Копировать/Copy
		/// </summary>
		public object Clone()
		{
			return new Note(Frequency, Duration);
		}

		/// <summary>
		/// хэш-функция/Hash code getter
		/// </summary>
		public override int GetHashCode()
		{
			var hashCode = -170734090;
			hashCode = hashCode * -1521134295 + Frequency.GetHashCode();
			hashCode = hashCode * -1521134295 + Duration.GetHashCode();
			return hashCode;
		}

		/// <summary>
		/// Сравнение/Equals
		/// </summary>
		public override bool Equals(object obj) => obj is Note note && this == note;

		/// <summary>
		/// Инверсия факта звучания ноты/Invert fact of note sounds 
		/// </summary>
		public static bool operator !(Note note) => note ? false : true;

		/// <summary>
		/// Нота может звучать/Note is not mute
		/// </summary>
		public static bool operator true(Note note) => note.Frequency >= 37 && note.Frequency <= 32767;

		/// <summary>
		/// Нота не может звучать/Note is mute
		/// </summary>
		public static bool operator false(Note note) => note.Frequency < 37 || note.Frequency > 32767;

		/// <summary>
		/// Обе ноты могут звучать/Both note is not mute
		/// </summary>
		public static Note operator &(Note note1, Note note2) => note1 ? (note2 ? note1 : new Note(0)) : new Note(0);

		/// <summary>
		/// Хотябы одна нота может звучать/at least one note is not mute
		/// </summary>
		public static Note operator |(Note note1, Note note2) => note1 ? note1 : (note2 ? note2 : new Note(0));

		/// <summary>
		/// Ноты равны/Notes are equal
		/// </summary>
		public static bool operator ==(Note note1, Note note2) => note1.Frequency == note2.Frequency && note1.Duration == note2.Duration;

		/// <summary>
		/// Ноты не равны/Notes are not equal
		/// </summary>
		public static bool operator !=(Note note1, Note note2) => !(note1 == note2);

		/// <summary>
		/// Частота 1 ноты больше частоты 2 ноты/Frequence of 1 note greater Frequence of 2 note
		/// </summary>
		public static bool operator >(Note note1, Note note2) => note1.Frequency > note2.Frequency;

		/// <summary>
		/// Частота 1 ноты больше или равно частоты 2 ноты/Frequence of 1 note greater or equal Frequence of 2 note
		/// </summary>
		public static bool operator >=(Note note1, Note note2) => note1.Frequency >= note2.Frequency;

		/// <summary>
		/// Частота 1 ноты меньше 2 ноты/Frequence of 1 note smaller of 2 note
		/// </summary>
		public static bool operator <(Note note1, Note note2) => note1.Frequency < note2.Frequency;

		/// <summary>
		/// Частота 1 ноты меньше или равно 2 ноты/Frequence of 1 note smaller or equal of 2 note
		/// </summary>
		public static bool operator <=(Note note1, Note note2) => note1.Frequency <= note2.Frequency;

		/// <summary>
		/// Умножение частоты ноты/Multiply frequence of note
		/// </summary>
		public static Note operator *(Note note, int k) => new Note(note.Frequency * k, note.Duration);

		/// <summary>
		/// Умножение частоты ноты/Multiply frequence of note
		/// </summary>
		public static Note operator *(int k, Note note) => note * k;

		/// <summary>
		/// Деление частоты ноты/Note frequency division
		/// </summary>
		public static Note operator /(Note note, int k) => new Note(note.Frequency / k, note.Duration);

		/// <summary>
		/// Деление частоты ноты/Note frequency division
		/// </summary>
		public static Note operator /(int k, Note note) => note / k;

		/// <summary>
		/// Сложение нот в мелодию/Adding notes to a melody
		/// </summary>
		public static Melody operator +(Note note1, Note note2) => new Melody(note1, note2);
	}
}
