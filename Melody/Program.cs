﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;

namespace Melody
{
	class Program
	{
        static void Main(string[] args)
        {
            //Создаём мелодию до рэ ми фа соль ля си до
            var melody1 = new Melody(Note.Do, Note.Re, Note.Mi, Note.Fa, Note.Sol, Note.La, Note.Si, new Note(523));

            //Проверка FindIndex с выходом за пределы диапаона
            var i1 = melody1.Contains(new Melody(Note.Do, Note.Re, Note.Mi, Note.Fa, Note.Sol, Note.La, Note.Si, new Note(523), new Note(523)));
            if (!i1) Console.WriteLine("Мелодия не найдена");

            //Проверка Submelody с выходом за пределы диапаона
            var melody4 = new Melody(Note.Do, Note.Do);
            var melody5 = melody4.SubMelody(0, 1);
            var melody6 = melody4.SubMelody(0, 2);
            var melody7 = melody4.SubMelody(1, 1);
            try
            {
                var melody8 = melody4.SubMelody(0, 3);//Тут получим исключение но уже из метода SubMelody
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            


            melody1.Play();

            //Проверяем вхождение мелодии из нот Ми Фа Соль в объект melody1
            if (melody1.Contains(new Melody(Note.Mi, Note.Fa, Note.Sol)))
                (!melody1)//Получаем мелодию в обратном порядке
                    .Play();//Воспроизводим. Должно получиться До си ля соль фа ми ре до

            //Создадим мелодию в лесу родилась ёлочка
            var melody2 = GetTheForestRaisedChristmasTreeMelodyPart1();

            //Получим индекс ноты 0 0
            var index0Note = melody2.FindIndex(new Note(0, 0));
            //Если нашли
            if (index0Note != -1)
                melody2[index0Note] = new Note(0, 500);//Заменим ноту

            var length = melody2.Count;

            //Сложение 2х мелодий
            melody2 += GetTheForestRaisedChristmasTreeMelodyPart2();

            //Используем расширение
            var melody3 = melody2.SubMelody(3, length);
            melody3.Play();
            melody2.SubMelody(length, melody2.Count).Play();

        }


        private static Melody GetTheForestRaisedChristmasTreeMelodyPart1()
        {
            //Пример сложения нот в мелодию
            var melody = new Note(247, 500) + new Note(417, 500);

            //Пример добавления ноты в мелодию
            melody += new Note(417, 500);
            melody += new Note(370, 500);
            melody += new Note(417, 500);
            melody += new Note(329, 500);
            melody += new Note(247, 500);
            melody += new Note(247, 500);
            melody += new Note(247, 500);
            melody += new Note(417, 500);
            melody += new Note(417, 500);
            melody += new Note(370, 500);
            melody += new Note(417, 500);
            melody += new Note(497, 500);
            melody += new Note(0, 0);
            return melody;
        }

        private static Melody GetTheForestRaisedChristmasTreeMelodyPart2()
        {
            var melody = new Melody();
            melody += new Note(497, 500);
            melody += new Note(277, 500);
            melody += new Note(277, 500);
            melody += new Note(440, 500);
            melody += new Note(440, 500);
            melody += new Note(417, 500);
            melody += new Note(370, 500);
            melody += new Note(329, 500);
            melody += new Note(247, 500);
            melody += new Note(417, 500);
            melody += new Note(417, 500);
            melody += new Note(370, 500);
            melody += new Note(417, 500);
            melody += new Note(329, 500);
            return melody;
        }


    }
}
