﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Melody
{
	/// <summary>
	/// Расширения класса Мелодии/Melody class extensions
	/// </summary>
	public static class MelodyExt
	{
		/// <summary>
		/// Получить мелодию/Get melody
		/// </summary>
		/// <param name="melody">Мелодия/Melody</param>
		/// <param name="startIndex">Индекс начала/Start index</param>
		/// <param name="lengtg">Длина/Length</param>
		/// <returns>Мелодия/Melody</returns>
		public static Melody SubMelody(this Melody melody, int startIndex, int lengtg)
		{
			var lastIndex = startIndex + lengtg;
			if (melody.Count < lastIndex) throw new IndexOutOfRangeException("melody");
			var subMelody = new Melody();
			for (var i = startIndex; i < lastIndex; i++)
				subMelody.Add(melody[i]);
			return subMelody;
		}
	}
}
