﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Melody
{
	/// <summary>
	/// Мелодия/Melody
	/// </summary>
	public class Melody
	{
		private List<Note> Notes => _notes ?? (_notes = new List<Note>());
		private List<Note> _notes;

		/// <summary>
		/// Количество/Count
		/// </summary>
		public int Count => Notes.Count;

		public Melody()
		{
		}

		public Melody(params Note[] notes) : this()
		{
			if (notes == null) 
				throw new ArgumentNullException("notes");
			Notes.AddRange(notes);
		}
		public Melody(Melody melody) : this()
		{
			if (melody == null) 
				throw new ArgumentNullException("melody");
			Notes.AddRange(melody.Notes);
		}

		/// <summary>
		/// Воспроизвести/Play
		/// </summary>
		public void Play()
		{
			Notes.ForEach(n => n.Play());
		}

		/// <summary>
		/// Добавить ноту в конец мелодии/Add note at the end of melody
		/// </summary>
		/// <param name="note">Нота/Note</param>
		/// <returns>Мелодия/Melody</returns>
		public Melody Add(Note note)
		{
			Notes.Add(note);
			return this;
		}

		/// <summary>
		/// Добавить малодию в конец мелодии/Add melody at the end of melody
		/// </summary>
		/// <param name="melody">Мелодия/Melody</param>
		/// <returns>Мелодия/Melody</returns>
		public Melody Add(Melody melody)
		{
			Notes.AddRange(melody.Notes);
			return this;
		}

		/// <summary>
		/// Найти индекс ноты в мелодии. При отстутствии вернёт -1/Find note index in melody. If not exists return -1
		/// </summary>
		/// <param name="note">Нота/Note</param>
		/// <returns>Индекс/Index</returns>
		public int FindIndex(Note note)
		{
			return Notes.FindIndex(x => x == note);
		}

		/// <summary>
		/// Найти индекс начала мелодии в мелодии. При отстутствии вернёт -1/Find melody start index in melody. If not exists return -1
		/// </summary>
		/// <param name="melody">Мелодия/Melody</param>
		/// <returns>Индекс/Index</returns>
		public int FindIndex(Melody melody)
		{
			if (melody.Count > Count) return -1;
			for (var i = 0; i < Count; i++)
			{
				for (var j = 0; j < melody.Count; j++)
				{
					if (this[i + j] != melody[j]) break;
					else if (j == melody.Count - 1) return i;
				}
			}
			return -1;
		}

		/// <summary>
		/// Проверка содержания мелодии в мелодии/Check containing melody in melody
		/// </summary>
		/// <param name="melody">Мелодия/Melody</param>
		/// <returns>Наличие мелодии/Melody exists</returns>
		public bool Contains(Melody melody)
		{
			return FindIndex(melody) != -1;
		}

		/// <summary>
		/// Нота/Note
		/// </summary>
		/// <param name="i">Индекс/index</param>
		/// <returns>Нота/Note</returns>
		public Note this[int i]
		{
			get => Notes[i];
			set => Notes[i] = value;
		}

		/// <summary>
		/// Добавить ноту в конец мелодии/Add note at the end of melody
		/// </summary>
		/// <param name="note">Нота/Note</param>
		/// <param name="note">Мелодия/Melody</param>
		/// <returns>Мелодия/Melody</returns>
		public static Melody operator +(Melody melody, Note note) => melody.Add(note);

		/// <summary>
		/// Добавить ноту в конец мелодии/Add note at the end of melody
		/// </summary>
		/// <param name="note">Нота/Note</param>
		/// <param name="note">Мелодия/Melody</param>
		/// <returns>Мелодия/Melody</returns>
		public static Melody operator +(Note note, Melody melody) => melody.Add(note);

		/// <summary>
		/// Добавить малодию в конец мелодии/Add melody at the end of melody
		/// </summary>
		/// <param name="melody1">Мелодия/Melody</param>
		/// <param name="melody2">Мелодия/Melody</param>
		/// <returns>Мелодия/Melody</returns>
		public static Melody operator +(Melody melody1, Melody melody2) => melody1.Add(melody2);

		/// <summary>
		/// Мелодия в обратном порядке/Reverse melody
		/// </summary>
		/// <param name="melody">Мелодия</param>
		/// <returns>Мелодия в обратном порядке/Reverse melody</returns>
		public static Melody operator !(Melody melody) => new Melody(melody.Notes.ToArray().Reverse().ToArray());
	}
}
